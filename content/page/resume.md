---
title: Resume

comments: false
---


### 4tiitoo

Internship • 2011

I did an internship at 4tiitoo for 12 weeks in Munich. During that time I’ve learned a lot about working in a team and the pitfalls in software development.

### Winner of the SAP competition //Enigma

UX Designer and Programmer • 2013

The Team //Enigma (Tom Langwaldt and I) won the 1st price as best software project sponsored by SAP Markdorf. We developed an Interface for tablets powered by Linux.

### Bachelor project: Connect

UX Designer and Programmer • 2016

A fast pacing racing game I developed with Patrick Mours, Fabian Beck, Marian Günthner. The design and artwork of the game was entirely made by me.

### Bachelor project: CyGo

UX Designer and Programmer • 2016

### Bachelor Thesis

Blueprint vs C++ • 2017

Speed comparison of Unreal Engines blueprints, Cooked Blueprint and native C++ code in terms of server-side performance.

### Winner of Virtual Reality Game competition Jam @SAE Stuttgart Wrekt

UX Designer and Programmer • 2016

Huge thanks to Epic, Jumping Llamas and SAE Stuttgart for the HTC Vive. The Team consisting of 3 Programmer (Tom Langwaldt, Daniel Schukies and I), one 3D Artists Barbara Mettler and one Sound Designer. Wrekt is about figuring out all ways how to move in VR. The player has to paddle from his wrecked (role credits) ship to the mainland. The challenge hereby is not to get eaten by hungry whales.


### ifm syntron Tettnang

GUI Development and Design • June 2018 - September 2018

C++/Qt GUI programming for various 2d/3d cameras and sensors

### Bosch grow platform GmbH Remotion Fitness VR Project

VR (Unreal Engine) and Webdevelopment • September 2018 -  Dezember 2018 

Part of the Bosch startup where we combined VR with cutting edge fitness.

### Haite GbR K3000 Office multiuser pipe management tool

Qt Programming • January 2019 -  *

Responsible to rebuild K3000, the successor of the 20 year old K2000. Build with multiuser functionality in mind the K3000 allows multiple people to work on a single project!


### Creator of ScreenPlay
Concept, Design, Programming, Documentation  • March 2017 - Now

[ScreenPlay](https://screen-play.app/) is an open source cross platform app for displaying Wallpaper, Widgets and AppDrawer. It is written in modern C++17/Qt5/QML. Binaries with workshop support are available for Windows and soon Linux & MacOSX via Steam. 

