---
title: About me

comments: false
---

Ever since I was a little kid I loved crafting and exploring all things technical. Later I grew a passion for Open Source and the humanism philosophy. In my teenage years I started using Photoshop, later Linux (via the WeTab since late 2010), build my own (and for some friends) PCs/NAS. I’ve learned  much about programming at the Claude-Dornier-Schule for Computer Science.

I founded my first company at the age of 18 that created homepages for local businesses. After my Abitur I moved to Stuttgart and studied Games Programming at the SAE Stuttgart with a Honours Thesis in "Speed comparison of Unreal Engines blueprints, Cooked Blueprint and native C++ code in terms of server-side performance." I now work (part time) at Haite as a Qt/c++ programmer and currently develop [ScreenPlay](https://screen-play.app/) as my main open source project.
