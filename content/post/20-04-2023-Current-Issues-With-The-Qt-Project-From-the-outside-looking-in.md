---
title: Current Issues With The Qt Project - From The Outside Looking In
date: 2023-04-20
---


*I want to start with that Qt is still IMHO the best way to develop C++ apps. This post is **not** supposed to be a rant. This is for the decision maker at Qt. If you have any question about Qt feel free to join our [/r/QtFramework](https://www.reddit.com/r/QtFramework/) subreddit!*

***The Qt developers are wonderful to work with, and every new Qt version makes it even better, thank you! 🎉***

My pain points (no particular order):


- [Collaboration](#collaboration)
- [Contributing and building](#contributing-and-building)
- [Daily builds](#daily-builds)
- [Dependency management](#dependency-management)
- [QtCreator](#qtcreator)
- [QML and QWidgets](#qml-and-qwidgets)
- [Qt needs to start using its own (qml based) products](#qt-needs-to-start-using-its-own-qml-based-products)
- [Onboarding of upcomming Qt developers](#onboarding-of-upcomming-qt-developers)
- [Is the Qt Company becomming another Oracle?](#is-the-qt-company-becomming-another-oracle)
- [Pricing](#pricing)
- [Current trends and the sharp increase in competition](#current-trends-and-the-sharp-increase-in-competition)
- [Closing thoughts](#closing-thoughts)

### Collaboration
![img](/images/blog/qt_cgit.png)
*Please, no more jira 🥲*

Qt currently uses a mix of Jira, Gerrit, Mailing lists, cgit, a custom CI coin and internal documents that are often linked in issues but not accessible for non Qt employees. For people who have used them for the last twenty years, this will work. It is this weird state of it is “good enough” so if you know what buttons to click, when to ssh into a remote gerrit, oh yes you have to use custom perl based scripts just to push things. It is weird how complex a simple line of code change can become. Thanks to *chatGPT* I have found the corresponding cognitive bias:

> “It's possible for something to appear to be "good enough" from the perspective of people who are already familiar with it, but seem less satisfactory to those who are not. This phenomenon is sometimes called the "curse of knowledge."
>
> The [curse of knowledge](https://en.wikipedia.org/wiki/Curse_of_knowledge) refers to the idea that people who are familiar with a particular concept, practice, or piece of information may have difficulty understanding how someone else who is not familiar with it might view it. This can lead to a situation where people who are already "in the know" may perceive something as good enough, while those who are not may view it as suboptimal or even unacceptable.
> 
> For example, imagine a group of people who are used to working with a particular software program that has a somewhat complicated interface. They might have become proficient at using it over time and may not see any problems with the interface. However, if someone who is not familiar with the software tries to use it, they might find the interface confusing or frustrating. In this case, the group of people who are familiar with the software may see it as good enough, while the outsider sees it as problematic.
> 
> In general, it's important to be aware of the curse of knowledge and to make an effort to consider how something might appear to people who are not already familiar with it. This can help ensure that practices, products, or services are designed in a way that is accessible and intuitive to a wider range of people.”

I have many small pain points with the tooling, like no mobile support for most of the tooling. Jira in general is just bad to work with, when even the most basic things like text formatting. [So my solution is something you already run yourself](https://git.qt.io) (internally) and many other projects in the FOSS community: Set up your own public Gitlab instance or use gitlab.com. This would unify all existing tooling and replace it with a consistent, well known workflow for outside contributors. Yes, this would mean to ax many of your own developed tools like coin, but look at the bright side, you can use the devs for other projects.

### Contributing and building
![img](/images/blog/qt_building.png)
*There`s got to be a better way that this...*

Let's get rid of the submodules and make Qt a mono repo. Managing dependencies in C++ is hard, but for example putting the QtPDF module into the QWebEngine repo only because of shared dependencies is weird (although I admire the pragmatism).  This would get rid of the Qt Submodule Update bot. I guess this would be feasible with CMake, CMakePresets.json to only build a small part of Qt at once. Let's always clone all code, but define what we want to compile in CMake. [Why is the qt git repo still called Qt5?](https://bugreports.qt.io/browse/QTQAINFRA-4200)

### Daily builds
![img](/images/blog/qt_snapshots.png)
*Where are my daily snapshots?*

We do not need snapshots, but daily Qt builds. I simply do not know if a snapshot contains changes that happen during the same day, or I have to wait weeks for a new snapshot to test if the bugs I reported are fixed. I would like to test all of my Qt app against these nightly builds, so I can report problems the day they are merged into Qt. Help me, help you.
### Dependency management
![img](/images/blog/vcpkg_link.png)
*[vcpkg.link](https://vcpkg.link/) is the best way to browse vcpkg packages*

Qt should create a vanilla c++ package and tooling manager like Rusts cargo for dependencies **and tools** (and make semantic versioning mandatory). This is needed for the next proposals I have below. I will be completely fine with a carbon copy of npm that you people host and get paid for the same way npm charges you for private packages or for premium features like CVE scanning of dependencies, prebuild binaries and license checks. Make the tool open source with vanilla c++ (aka non Qt projects), only so it can become the de facto standard. This will be free advertising and an additional revenue stream.


### QtCreator
![img](/images/blog/qtcreator-qml.png)
*This does not inspire confidence*

Why is QtCreator still not using QML for new code? Does the Qt Company not have confident in it? Also some other points:
1. Make more things json/toml/text settings based instead of UI based, like *tasks.json* in VSCode that allows users to execute simple tasks from the UI. The main advantage is that it will work for every user that opens the projects. Things like the new CMakePresets.json are great to get rid of the manual kit settings, that every developer had to change. This is still so fragile and bug prone.
1. Allow python based plugins and make them available via the above mentioned dependency management tool. The current way to load qt plugins by compiling it for every platform is not feasible if you have an idea about a nice little helper plugin.
1. Add live reloading to qml projects (No, the current QtCreator implementation is not usable). This is the default developing experience for web projects, or even Qt competitors like Flutter.
1. Add a developer toolbar into the app itself. I’m thinking about the same developer tools that web developers have in their browser, with an inspector and live changes like changing the color of a control. It has been 10 years and qml still has not shipped with the rapid prototyping promise. Making a change, compiling qml, starting the app and then navigation into the exact location of the feature one is developing to see the changes is not rapid prototyping. I want to see changes instantly. You even deprecated the only way we would have had the slightest form of rapid prototyping with the qml designer. The argument was that the designer was brittle because it had to support different Qt/QML versions at the same time. This is true, but you would not have this problem if you have the tool I described above.

### QML and QWidgets
It feels there is an eternal fight between users that use QWidgets and users that use qml. Qml is the new (more than 10 years old at this time) shiny tool that still lacks behind in many aspects and QWidgets is “good enough” for most applications and are even highly preferred by many because of the QML === JavaScript stigma. This is not only visible in the community, but in the way Qt itself creates its tools. Let's list all tools from Qt itself that use QWidgets

- QLinguist
- QtCreator - Let's ignore the qml editor part that is written in qml, still the vast majority of new code is still written in QWidgets. Even things like the welcome page were once rewritten in qml and then replaced again with widgets because back then qml was not mature enough.
- QtInstallerFramework UI - The tools that would be perfect for qml, but still it uses the deprecated QScript, and it cumbersome to work with.
- …?

How many Qt tools use exclusively QML? I don't know a single one. I know Qt is in a difficult position here. We do not want to deprecate QWidgets because my guess is, that still the vast majority of apps are written in QWidgets. So qml cannot be the future of Qt. This caused me to have some hilarious experiences with teams in the last years when they started to develop their app in Qt but with QWidgets and QML **at the same time,** because qml still has the stigma of being mobile only and QWidget apps “are desktop only”.

Write once, deploy everywhere? [NotLikeThis.jpg](https://www.twitchmetrics.net/e/58765-NotLikeThis)

Now we have a mix oft two technologies that you can choose, that both can do cross-platform apps, but both have advantages and disadvantages. So what do you tell people to choose?

pyside6 is another example. [The quick start guide tells you to use QWidgets](https://doc.qt.io/qtforpython-6/quickstart.html#create-a-simple-qt-widgets-application). For a new way to write Qt apps, this does not make me confident that QML is the future of Qt.


### Qt needs to start using its own (qml based) products
I once worked for a startup for feedback called [stomt](https://www.stomt.com/). Imagine Twitter for feedback where every sentence starts either with “I like X because” and “I wish X would”. Then an external contractor who we asked how we could improve our app said the most obvious thing nobody thought about: “How about you use stomt, for stomt?” You know that feeling when someone tells you something so obvious, but it’s true?

My point here is you will know what you need if you use your own product (like Mitch Curtis did with Slate). Develop any kind of app and sell it. I don’t know, create a cross-platform app like Microsoft Word. Then you will have to do all the things we customers have to deal with. What you need a TrayIcon, but it is still in the QtQmlLabs modules, and it has been experimental for the last 10 years? Oh, you need an installer to deliver your app, but Qt Installer Framework only supports a weird JavaScript dialect? You get the idea.

### Onboarding of upcomming Qt developers
![img](/images/blog/qt_open_source.png)

Qt onboarding is just too much. I will say that all of www.qt.io is a result of pushing one into buying a qt license. Don’t get me wrong, I want the Qt company to have many paying customers because everyone would profit from that. This just means that it is deliberately confusing, so people thought, they can use Qt free of charge.

The most questions we get in [/r/QtFramework](https://www.reddit.com/r/QtFramework/):

1. Am I allowed to use Qt for my App/Startup/University project?
1. Should I use Widgets or QML? (Already answered above)

The answer to 1 is usually: *Yes, but…* . Then it will follow with a big list to keep in mind what you are required to do. It makes things complicated. Other frameworks are not (See current trends below).

pyside6 is an example of good onboarding, with a simple “pip install pyside6” it already tells me everything that I can expect. No registration, no bullshit, just one command and you are good to go.

Now I have to create an account, have to log in just to download Qt on all of our workstations? No, thank you, we simply switched to [aqt](https://github.com/miurahr/aqtinstall) for prebuild binaries. No other framework mentioned below will force (except Unreal Engine and Unit3d AFAIK) you to do this.

### Is the Qt Company becomming another Oracle?
![img](/images/blog/qt-offering-changes-2020.png)
*[People are angry 👀](https://www.qt.io/blog/qt-offering-changes-2020#commento)*

Over the years, the Qt Company, once widely known for its dedication to open-source software and the developer community, has gradually shifted its focus towards enterprise customers. Firstly, the company has increasingly focused on revenue generation, leading to a stronger emphasis on enterprise clients. These clients typically have deeper pockets and are willing to pay for premium support and services. While it is totally fine for a company wanting to make money, it is frustrating that we do not have an option for a more flexible license. As a result, the Qt Company has started prioritizing their needs, often at the expense of smaller clients and open-source enthusiasts.

### Pricing
![img](/images/blog/qt_pricing.png)
*3.6$ per developer/year. How about flutters 0$/year?*

I have already said it many times online. The way Qt wants to make money is too much focused in big B2B style. There is a general sentiment in the community that the current pricing is bad for small businesses. It feels like giving into a trap when using Qt for small business. [Let me quote myself](https://www.reddit.com/r/QtFramework/comments/lwt3r9/comment/gpj1qwn/?utm_source=share&utm_medium=web2x&context=3):

> *I want to use Qt. I want to pay for Qt. I really want to use everything that Qt has to offer. The software is not the problem here, only the management. I'm salty about this because the Qt people show us some really great software that I cannot use because of this.*

EU Digital Markets Act, will force all gatekeeper (defines as all companies with sufficient market power like Apple) to open up restricted operating systems for other app stores and sideloading. It is rumored that [iOS 17 will introduce sideloading](https://www.macrumors.com/2023/04/17/app-sideloading-support-coming-ios-17/) this summer. This means I will be able to sell Qt apps on Apple the same way as I do on Android. This will potentially mean more people switching to LGPL version of Qt. But this is not what we want (me included). We want people to pay a fair price for Qt.

My point is that Qt needs a better pricing model to stay relevant. I don’t want to pay thousands of dollars before I even start to ship my app. I want zero upfront cost and give you a share of my revenue, like EPIC Games does with Unreal Engine.

Click on that link, <https://www.unrealengine.com/en-US/license> and allow us to use Qt for the same price.

- A fixed price with ALL of the modules and tools, no restriction.
- A fixed 5% percentage of the revenue with ALL of the modules and tools, no restrictions. In the end this will potentially even generate more revenue for you. These 5% are acceptable when compared to the 30% that stores like Steam or Apple take from you.

Way too often Qt announces a shiny new tool to play with (looking at you QtQuick3d or WebAssembly) that I’m not able to use, only because of licensing restrictions. This even forced us to embed a web export of Godot into our Qt app to have 3d functionality via the QWebEngine, because we would not have to deal with the Qt licensing shenanigans.


### Current trends and the sharp increase in competition
Google “best cross platform framework” and the first 6 results do not even mention Qt (let's ignore the big ad space Qt bought when googleing this exact query. I've installed adblocker on all of my devices except the iPad I used for searching this, lol). 

![img](/images/blog/best-cross-platform-frameworks.png)
*Source: [statista](https://www.statista.com/statistics/869224/worldwide-software-developer-working-hours/). I know this is mobile, but still...*


This list does not contain a single C++ library. With the current trend to a more safer language and a general trend that they simply stopped teaching people C++ in favor of python, csharp or java. Qt in its core is a C++ and a GUI library, but pyside6 has shown that we can write applications with better languages than C++, although mobile support is still missing to call it even an alternative to C++. IMHO Qt needs to support even more languages (with mobile support), because of outside factors like slow development of C++ features. I don’t want to sound like all other Rust evangelists, but it does need Rust support/bindings to stay relevant. I for example will not use C++ for my next project, Rust ist just on another level regarding language design, dependency management and tooling.


### Closing thoughts
Would I start a new project with Qt? Maybe? I don`t know. This is because of all of the shenanigans the Qt Company does, but also because of my growing dislike of the way C++ is not getting any better (this still 404s for me 🧐: [www.change.org/break-cpp-abi-already-ffs-its-2023](https://www.change.org/break-abi-already-ffs-its-2023)).  Things have changed and there are so many ok enough cross platform frameworks, that are 100% free. I would use Flutter or React Native in combination with the core logic being written in Rust I guess? 🦀

