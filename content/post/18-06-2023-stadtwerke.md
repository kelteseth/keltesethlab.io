---
title: "Stadtwerke, Elektroautos und die grüne Wende: Eine Analyse"
date: 2023-06-18
---

Die Diskussion um die Durchführbarkeit der grünen Wende ist mittlerweile allgegenwärtig. Gerade unsere [Stadtwerke](https://www.stadtwerk-am-see.de/) spielen dabei eine entscheidende Rolle. Aber wie grün sind sie wirklich?

Die Zahlen aus dem Jahr 2022 lassen zu wünschen übrig: 
* 22% Erneuerbare
* 14% Erdgas
* 2% sonstige Fossile
* 43% Kohle
* 20% Kernkraft
  
Mit 60% fossiler Energie und 496g/kWh ist der CO2-Fußabdruck größer als der deutsche Durchschnitt von 350g/kWh. Viele argumentieren, dass es unmöglich sei, vollständig auf erneuerbare Energien umzustellen, und es besser wäre, ein "Slow Mover" zu sein. Aber ist das wirklich eine nachhaltige Haltung? Die große Frage ist immer, was ist, wenn kein Wind weht und keine Sonne scheint?  Welche Dimensionen an Energiespeicher würden wir (im Bodenseekreis) benötigen?

### Die Rolle der Elektroautos

Bis 2030 sollen laut [Prognosen 25% der Autos auf unseren Straßen Elektroautos sein](https://de.statista.com/statistik/daten/studie/1202904/umfrage/anteil-der-elektroautos-am-pkw-bestand-in-deutschland/). Der Bodenseekreis mit seinen 220.000 Einwohnern und einer Autodichte von 75% könnte dann etwa 41.250 Elektroautos beheimaten. Ein Tesla Model 3 verfügt beispielsweise über eine Batteriekapazität von 75 kWh. Wenn man diese auf die Anzahl bis 2030 hochrechnet, wäre dies der ideale dezentraler Energiespeicher.

[Ein privater Pkw wird durchschnittlich nur eine Stunde am Tag bewegt](https://www.umweltbundesamt.de/themen/verkehr/nachhaltige-mobilitaet/car-sharing). Wieso also nicht das Auto als Energiespeicher nutzen?

{{< iframe src="/post/18-06-2023-stadtwerke-iframe.html" width="100%" height="1000px" frameborder="0" >}}


Die Menge der prognostizierten Elektroautos würde ausreichen, um die gesamte Region für etwa 1,7 Tage mit Strom zu versorgen, ohne zusätzlichen Strom zu produzieren. Natürlich ist es unwahrscheinlich, dass alle Autos gleichzeitig vollständig entladen sind und keine Stromproduktion stattfindet. Aber es zeigt das Potential der E-Mobilität nicht nur als Verkehrsmittel, sondern auch als Energiespeicher.


### Der Ausbau von Wind- und Solarenergie
Um die benötigte Energiemenge ausschließlich durch erneuerbare Energien zu erzeugen, wären erhebliche Investitionen in Solarenergie und Windkraft erforderlich. Eine durchschnittliche 5 kWp Solaranlage in Deutschland produziert etwa 5.000 kWh pro Jahr. Um die benötigten 2.073.972 kWh pro Tag zu erzeugen, würden etwa 151.000 solcher Anlagen benötigt, was rund 1,8 Millionen Solarpanels entspricht. Für die Windkraft gibt es vom Land Baden-Württemberg eine [Karte](https://www.energieatlas-bw.de/wind/windatlas), die die potenzielle Windkraft zeigt.


![img](/images/blog/windatlas-baden-wurttemberg.png)
> Ein Standort gilt als ausreichend Windhöffig, wenn mittlere gekappte Windleistungsdichte ≥ 215 W/m²: [Windbedingungen am Standort Stiftäcker (Seite 8)](https://www.rheinstetten.de/files/rhs-internet-presse---nur-administratoren/windenergie/windenergie-rentabilitaet-praesentation.pdf)

![img](/images/blog/ermittelte-windpotenzialflaechen.png)
Wie wir sehen können, lohnt es sich ja doch hier Windräder im Bodenseeraum zu bauen. Verrückt. [Woher kommt denn immer das Stigma es wurde sich nur im Norden lohnen](https://link.springer.com/article/10.1007/s10010-023-00671-w)? 

[Naturlich sind es wieder die Klimaleugner.](https://lobbypedia.de/wiki/Bundesinitiative_Vernunftkraft)
> Die Bundesinitiative Vernunftkraft e. V. (Vernunftkraft) ist ein Dachverband von Anti-Windkraft-Initiativen und ihren Landesverbänden, der sich für die Abschaffung des Erneuerbare-Energien-Gesetzes (EEG) und den Stopp des Ausbaus von Windkraft und Photovoltaik einsetzt. Dagegen sollen Kohle- und Kernkraftwerke weiter genutzt werden. Die Argumentationsmuster der Vernunftkraft-Repräsentanten stimmen weitgehend mit denen überein, die von Leugnern des menschengemachten Klimawandels (Klimaleugner) und deren Organisationen vertreten werden. Politisch unterstützt wird Vernunftkraft von der AfD und Teilen der FDP, insbesondere dem FDP-Landesverband Hessen.

In Bezug auf Windenergie könnte ein [Siemens Windrad](https://www.siemensgamesa.com/en-int/products-and-services/onshore/wind-turbine-sg-7-0-170) etwa 7.000 kWh pro Tag erzeugen. Um den gesamten täglichen Bedarf zu decken, würden also mindestens 300 solcher Windräder benötigt bei 100% Leistung. Das ist natürlich nicht realistisch, deswegen brauchen wir Informationen, wie stabil der Wind in Deutschland weht (siehe [Herausforderungen bei Wind- und Solarenergie](#herausforderungen-bei-wind--und-solarenergie)).

### Winkraftlagen in Vorarlberg
Die Vorarlberger Windkraftpotenzialerhebung 2023 hat [kürzlich sechs Schlüsselregionen identifiziert](https://vorarlberg.orf.at/stories/3203454/), die sich wahrscheinlich als rentabel für die Nutzung von Windenergie erweisen würden. Dazu gehören der Pfänderrücken, der Bregenzerwald, die Allgäuer Alpen, die Lechtaler Alpen, Rätikon-Silvretta und die Region Feldkirch. Laut Studie, die von der Energiewerkstatt durchgeführt wurde, könnten diese Regionen aufgrund ihrer durchschnittlichen Windstärke, Hangneigung und Untergrundbeschaffenheit Windenergie effektiv nutzen. on den genannten Regionen weisen der Pfänderrücken und der Bregenzerwald die höchsten Leistungsdichten auf, während Gebiete wie die Lechtaler Alpen und Rätikon-Silvretta aufgrund ihres alpinen Charakters und steilen Hangneigungen Herausforderungen darstellen könnten.


![img](/images/blog/Windpotential-Vorarlberg_2023.png)

### Herausforderungen bei Wind- und Solarenergie
Die Zuverlässigkeit von Wind- und Solarenergie ist ein häufig genanntes Argument gegen den vollständigen Umstieg auf erneuerbare Energien. Ein wissenschaftlich Studie in "Nature" zeigt, dass Solar- und Windenergie in Ländern der Nordhalbkugel konsistent im Sommer und Winter ihren Höhepunkt erreichen. Das bedeutet, dass es Jahreszeiten gibt, in denen die Produktion von erneuerbaren Energien geringer ist. Darüber hinaus kann die Windenergieproduktion von Jahr zu Jahr stark variieren, was zusätzliche Unsicherheit in die Energieversorgung bringt. Deswegen müssen wir verschiedenste Arten von Energiespeicher schaffen, um diese zu überbrücken.


![img](/images/blog/reliability-of-solar-and-wind.png)
[Geophysical constraints on the reliability of solar and wind power worldwide](https://www.nature.com/articles/s41467-021-26355-z)


> Solar and wind consistently peak in summer and winter, respectively, in countries of the Northern Hemisphere. However, during the 39-year period, interannual variability of wind is consistently much greater than that of solar in most countries (Fig. 1a–f), though the magnitude of these resources’ variability differs substantially between two particular countries. For example, Germany’s small area (0.36 million km2) and high latitude (centroid 51.2 °N) result in large interannual variations in both solar (measured by the robust coefficient of variation23; RCoV = 58.8%) and wind resources (RCoV = 47.2%, Fig. 1b),



### Die Zeit drängt

#### Hitze-, Umwelt- und Klima-Update Mai 2023
{{< iframe src="https://www.youtube.com/embed/RPs9JdthcHU?start=685" width="720" height="415" frameborder="0" >}}

#### Reinhard Steurer - Scheinklimaschutz: Eine Bestandsaufnahme zur Klimakrise
{{< iframe src="https://www.youtube.com/embed/XhGpU26D02I?start=41" width="720" height="415" frameborder="0" >}}

### Aktuelle Entwicklung (17.06.2023)
[Die EU-Mitgliedsstaaten haben sich auf ein Gesetz geeinigt](https://www.zeit.de/politik/ausland/2023-06/europaeische-union-erneuerbare-energien-gesetz), das einen Ausbau der erneuerbaren Energien vorsieht, um einen größeren Anteil des Energieverbrauchs der EU durch erneuerbare Quellen zu decken. Der jüngste Konflikt über das Gesetz wurde beigelegt, nachdem Frankreich Ausnahmeregelungen für einige Ammoniakwerke aushandelte. Laut dem Gesetz sollen bis 2030 42,5 Prozent der in der EU verbrauchten Energie aus erneuerbaren Quellen wie Wind-, Solar- oder Wasserkraft stammen, wobei das freiwillige Ziel bei 45 Prozent liegt. Dies bedeutet einen deutlichen Anstieg gegenüber dem bisherigen Ziel von 32 Prozent. Darüber hinaus müssen bis 2030 42 Prozent und bis 2035 60 Prozent des in der Industrie verwendeten Wasserstoffs aus erneuerbaren Quellen stammen, wobei Ausnahmeregelungen für Länder mit geringem Anteil an fossilen Brennstoffen vorgesehen sind.


### Meine 10 Fragen an Standtwerk am See
Diese Fragen beziehen sich auf die [Umwelterklärung 2022](https://www.stadtwerk-am-see.de/de/Ueber-uns/Portraet/Nachhaltigkeit-und-Oekologie/Umwelterklaerung-Stadtwerk-am-See.pdf).

1. Der Grafik auf Seite 9 zu entnehmen gibt es eine Beteiligung an mehreren Solar und Windparks. Welche Pläne gibt es für deren Ausbau um die bis 2030 benötigte CO2 Reduzierung von 50% zu erreichen?
2. Hat sich die Planung von neuen Windkraftanlagen geändert nachdem nun die Windkraft als kritische Infrastruktur zählt?
3. Welche konkrete Pläne gibt es um die Co2 Bilanz bis 2045 auf Null zu senken?
4. Welche aktuellen Investition gibt es in neue Projekte die nicht Klimaneutrale Techniken verwenden?
5. Was tun Sie wegen der Abschaltung von Solaranlagen und Windkraftanlagen aufgrund mangelnder Speichermöglichkeiten. Sind Batteriespeicher oder 1. Wasserstoffanlagen zur Herstellung von Wasserstoff bei negativen Strompreis in Planung?
6. Sind die Spitzenlastaggregate die auf Seite 19 erwähnt werden Dieselgeneratoren? Gibt es Plane diese durch Erneuerbare Speicher zur ersetzen?
7. Ist das Holz der  Holzhackschnitzelkessel extra angelegter Wald oder besteht das Holz aus Holzabfällen?
8. Der Grafik auf Seit 31 ist zu entnehmen das der Gesamtstrommix der Stadtwerke am See mit 496 g/kwh größer ist als der Durchschnitt in Deutschland mit 350 g/kwh. Wieso?
9.  Ihr Ziel ist es die gesamte Kommunikation mit den Kunden auf Online umzustellen (Seite 32.), aber die Anzahl der verwendeten Seiten ist 2021 sogar gestiegen. Wie viele Jahre planen Sie das der Umstieg auf Email noch dauern wird?
10. Kann ich Ihnen meine vielen Flachdachflächen zur Verfügung stellen (Miete?) zum errichten von Solaranlagen. Ich habe nur https://www.stadtwerk-am-see.de/energiedach gefunden aber wie ich das sehen ist nur wenn ich ein Solardach selber errichten und Finanzieren will. Das ist bei vielen Menschen in Friedrichshafen finanziell nicht möglich. Diese konnten aber ihre Dächer für Photovoltaik zu Verfügung stellen. 